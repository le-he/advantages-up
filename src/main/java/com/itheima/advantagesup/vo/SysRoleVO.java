package com.itheima.advantagesup.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * SysRoleVO
 *
 * @author liudo
 * @version 1.0
 * @project advantages-up
 * @date 2023/9/11 21:26:47
 */
@Data
public class SysRoleVO {
    private String id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色描述
     */
    private String description;

    /**
     * 创建时间
     */
    private Date create_date;

    /**
     * 更新时间
     */
    private Date update_date;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记
     */
    private String del_flag;

    /**
     * 是否为超级管理员
     */
    private Integer superadmin;

    /**
     * 默认数据
     */
    private String default_data;

    /**
     * 角色菜单
     */
    private String role_menus;

    /**
     * 身份验证列表
     */
    private List<MenuVO> authList;
}
