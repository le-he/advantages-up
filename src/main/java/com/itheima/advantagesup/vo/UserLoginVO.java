package com.itheima.advantagesup.vo;

import lombok.Builder;
import lombok.Data;

/**
 * UserLoginVO
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 16:48
 */
@Data
@Builder
public class UserLoginVO {

    private String token;
}
