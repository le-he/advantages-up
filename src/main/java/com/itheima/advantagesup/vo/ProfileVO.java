package com.itheima.advantagesup.vo;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * ProfileVO
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 17:18
 */
@Data
public class ProfileVO {

    /**
     * 用户信息
     */
    private UserInfoVO userInfo;
    /**
     * 菜单列表
     */
    private List<MenuVO> menuList;
    /**
     * 字典列表
     */
    private Map<String, Object> dictsList;
    /**
     * btn列表
     */
    private List<String> btnList;
}
