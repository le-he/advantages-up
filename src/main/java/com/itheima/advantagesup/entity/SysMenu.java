package com.itheima.advantagesup.entity;

import com.itheima.advantagesup.vo.MenuVO;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author zhengkai.blog.csdn.net
 * @description menu
 * @date 2023-07-26
 */
@Data
public class SysMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Integer id;

    /**
     * 父级编号
     */
    private Integer pid;

    /**
     * 路由名称
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 菜单名称
     */
    private String title;

    /**
     * 图标
     */
    private String icon;

    /**
     * 是否在菜单中显示，0不显示，1显示
     */
    private Integer isShow;

    /**
     * 是否缓存，0否，1是
     */
    private Integer isCache;

    /**
     * 是否外联，0否，1是
     */
    private Integer isLink;

    /**
     * 重定向地址
     */
    private String redirect;

    /**
     * 创建时间
     */
    private LocalDateTime createDate;

    /**
     * 更新时间
     */
    private LocalDateTime updateDate;

    /**
     * 删除标记
     */
    private Integer delFlag;

    /**
     * 类型，1是菜单，2按钮
     */
    private Integer type;

    /**
     * 排序
     */
    private BigDecimal sort;

    /**
     * 权限标识
     */
    private String mark;

    /**
     *默认日期
     */
    private String defaultData;

    public MenuVO transformVO() {
        MenuVO menuVO = new MenuVO();
        menuVO.setId(this.id);
        menuVO.setPid(this.getPid());
        menuVO.setPath(this.path);
        menuVO.setComponent(this.component);
        menuVO.setTitle(this.title);
        menuVO.setIcon(this.icon);
        menuVO.setIs_show(this.isShow);
        menuVO.setIs_cache(this.isCache);
        menuVO.setIs_link(this.isLink);
        menuVO.setCreateDate(this.createDate);
        menuVO.setUpdateDate(this.updateDate);
        menuVO.setDel_flag(this.delFlag);
        menuVO.setType(this.type);
        menuVO.setSort(this.sort);
        menuVO.setMark(this.mark);
        return menuVO;
    }
}