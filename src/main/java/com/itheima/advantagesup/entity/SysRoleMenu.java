package com.itheima.advantagesup.entity;

import lombok.Data;

/**
 * @author liudo
 * @version 1.0
 * @project advantages-up
 * @date 2023/9/11 21:44:55
 */
@Data
public class SysRoleMenu {

    /**
     * 角色id
     */
    private String roleId;
    /**
     * 菜单id
     */
    private Integer menuId;
}
