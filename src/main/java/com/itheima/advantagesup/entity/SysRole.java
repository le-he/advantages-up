package com.itheima.advantagesup.entity;

import com.itheima.advantagesup.vo.SysRoleVO;
import lombok.Data;

import java.util.Date;

/**
 * @author liudo
 * @version 1.0
 * @project advantages-up
 * @date 2023/9/11 21:20:48
 */
@Data
public class SysRole {

    private String id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色描述
     */
    private String description;

    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 更新时间
     */
    private Date updateDate;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记
     */
    private String delFlag;

    /**
     * 是否为超级管理员
     */
    private Integer superadmin;

    /**
     * 默认数据
     */
    private String defaultData;

    public SysRoleVO covertToVO() {
        SysRoleVO sysRoleVO = new SysRoleVO();
        sysRoleVO.setId(this.id);
        sysRoleVO.setName(this.name);
        sysRoleVO.setDescription(this.description);
        sysRoleVO.setCreate_date(this.createDate);
        sysRoleVO.setUpdate_date(this.updateDate);
        sysRoleVO.setRemarks(this.remarks);
        sysRoleVO.setDel_flag(this.delFlag);
        sysRoleVO.setSuperadmin(this.superadmin);
        sysRoleVO.setDefault_data(this.defaultData);
        return sysRoleVO;
    }
}
