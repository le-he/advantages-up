package com.itheima.advantagesup.exception;

import com.itheima.advantagesup.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * GlobleExceptionHandler
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 18:55
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 异常
     *
     * @param exception 异常
     * @return {@link R}<{@link ?}>
     */
    @ExceptionHandler(Exception.class)
    public R<?> exception(Exception exception) {
        log.error(exception.getMessage(), exception);
        return R.error("未知异常");
    }

    /**
     * 异常
     *
     * @param exception 异常
     * @return {@link R}<{@link ?}>
     */
    @ExceptionHandler(BusinessException.class)
    public R<?> exception(BusinessException exception) {
        log.error(exception.getMessage(), exception);
        return R.error(exception.getCode(), exception.getMessage());
    }


}
