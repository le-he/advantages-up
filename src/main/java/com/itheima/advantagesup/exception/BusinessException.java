package com.itheima.advantagesup.exception;

import lombok.Getter;

/**
 * BusinessException
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 18:57
 */
@Getter
public class BusinessException extends RuntimeException{

    private int code = 1000;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String message, int code) {
        super(message);
        this.code = code;
    }
}
