package com.itheima.advantagesup.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 页面bean
 * 分页对象
 *
 SysRole * @project advantages-up
 * @date 2023/9/11 21:25:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageBean<T> {

    /**
     * 计数
     */
    private Long count;
    /**
     * 总页数
     */
    private Long totalPages;
    /**
     * 页面大小
     */
    private Long pageSize;
    /**
     * 当前页面
     */
    private Long currentPage;
    /**
     * 数据
     */
    private List<T> data;

    public static <T> PageBean<T> of(Long count, Long totalPages, Long pageSize, Long currentPage, List<T> data) {
        return new PageBean<>(count, totalPages, pageSize, currentPage, data);
    }

    public static <T> PageBean<T> of(Long count, List<T> data) {
        return PageBean.of(count, null, null, null, data);
    }
}
