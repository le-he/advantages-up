package com.itheima.advantagesup.common;

/**
 * BaseContext
 *
 * @author liliudong
 * @version 1.0
 * @description 上下文
 * @date 2023/7/26 17:13
 */
public class BaseContext {

    private final static ThreadLocal<String> THREAD_LOCAL = new ThreadLocal<>();

    /**
     * 设置登录用户id
     *
     * @param userId 用户id
     */
    public static void setLoginUserId(String userId) {
        THREAD_LOCAL.set(userId);
    }

    /**
     * 获得登录用户id
     *
     * @return {@link String}
     */
    public static String getLoginUserId() {
        return THREAD_LOCAL.get();
    }

    /**
     * 删除登录用户id
     */
    public static void removeLoginUserId() {
        THREAD_LOCAL.remove();
    }

}
