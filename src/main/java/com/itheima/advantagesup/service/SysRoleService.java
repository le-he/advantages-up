package com.itheima.advantagesup.service;

import com.itheima.advantagesup.common.PageBean;
import com.itheima.advantagesup.dto.SysRoleDTO;
import com.itheima.advantagesup.entity.SysRole;
import com.itheima.advantagesup.vo.SysRoleVO;

import java.util.List;

public interface SysRoleService {

    PageBean<SysRoleVO> list(Integer page, Integer limit, String name);

    List<SysRoleVO> allData();

    void add(SysRoleDTO sysRoleDTO);



    SysRoleVO rowInfo(String id);

    void update(SysRoleDTO sysRoleDTO);

    void del(String id);
}
