package com.itheima.advantagesup.service;

import com.itheima.advantagesup.dto.UserLoginDTO;
import com.itheima.advantagesup.vo.ProfileVO;
import com.itheima.advantagesup.vo.UserLoginVO;

/**
 * IndexService
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 17:48
 */
public interface IndexService {
    /**
     * 登录
     *
     * @param userLoginDTO 用户登录dto
     * @return {@link UserLoginVO}
     */
    UserLoginVO login(UserLoginDTO userLoginDTO);


    ProfileVO profile();
}
