package com.itheima.advantagesup.service.impl;

import com.itheima.advantagesup.dto.SysMenuDTO;
import com.itheima.advantagesup.entity.SysMenu;
import com.itheima.advantagesup.mapper.SysMenuMapper;
import com.itheima.advantagesup.service.SysMenuService;

import com.itheima.advantagesup.vo.MenuVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SysMenuServiceImpl implements SysMenuService {

    private final SysMenuMapper sysMenuMapper;


    @Override
    public void add(SysMenu sysMenu) {
        sysMenuMapper.insert(sysMenu);
    }

    @Override
    public void delete(Integer id) {
        sysMenuMapper.delete(id);
    }

    @Override
    public void update(SysMenu sysMenu) {
        sysMenuMapper.update(sysMenu);
    }
    @Override
    public SysMenu selectById(Integer id) {
        return sysMenuMapper.selectById(id);

    }

    @Override
    public List<MenuVO> allData() {
        List<SysMenu> sysMenuList = sysMenuMapper.selectListByPid(0);
        return sysMenuList.stream().map(this::getChildren).collect(Collectors.toList());
    }



    private MenuVO getChildren(SysMenu sysMenu) {
        List<SysMenu>  children  = sysMenuMapper.selectListByPid(sysMenu.getId());
        if(CollectionUtils.isEmpty(children)){
            return sysMenu.transformVO();
        }
        List<MenuVO> menuVOList = children.stream().map(this::getChildren).collect(Collectors.toList());
        MenuVO menuVO = sysMenu.transformVO();
        menuVO.setChildren(menuVOList);
        return menuVO;
    }
}
