package com.itheima.advantagesup.service.impl;

import com.itheima.advantagesup.service.SysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * SysUserServiceImpl
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 14:58
 */
@Service
@RequiredArgsConstructor
public class SysUserServiceImpl implements SysUserService {


}
