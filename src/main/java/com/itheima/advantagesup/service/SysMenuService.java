package com.itheima.advantagesup.service;

import com.itheima.advantagesup.dto.SysMenuDTO;
import com.itheima.advantagesup.entity.SysMenu;
import com.itheima.advantagesup.vo.MenuVO;

import java.util.List;

public interface SysMenuService {


    void add(SysMenu sysMenu);

    void delete(Integer id);

    void update(SysMenu sysMenu);

    List<MenuVO> allData();

    SysMenu selectById(Integer id);
}
