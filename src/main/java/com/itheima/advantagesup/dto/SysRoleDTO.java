package com.itheima.advantagesup.dto;


import lombok.Data;

import java.util.List;

@Data
public class SysRoleDTO {
    private String id;

    private String name;

    private String description;

    private List<Integer> menudis;
}
