package com.itheima.advantagesup.dto;

import lombok.Data;

import java.math.BigDecimal;

/*"component": "string",
        "icon": "string",
        "is_cache": "string",
        "is_link": "string",
        "is_show": "string",
        "mark": "string",
        "path": "string",
        "pid": "string",
        "redirect": "string",
        "sort": "string",
        "title": "string",
        "type": "string"*/
@Data
public class SysMenuDTO {
    private Integer id;
    private String component;
    private String icon;
    private Integer is_cache;
    private Integer is_link;
    private Integer is_show;
    private String mark;
    private String path;
    private Integer pid;
    private String redirect;
    private BigDecimal sort;
    private String title;
    private Integer type;



}
