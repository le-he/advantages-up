package com.itheima.advantagesup.dto;

import lombok.Data;

/**
 * UserLoginDTO
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 16:47
 */
@Data
public class UserLoginDTO {

    /**
     * 登录名
     */
    private String login_name;
    /**
     * 密码
     */
    private String password;
}
