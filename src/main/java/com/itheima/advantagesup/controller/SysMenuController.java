package com.itheima.advantagesup.controller;

import com.itheima.advantagesup.common.R;
import com.itheima.advantagesup.dto.SysMenuDTO;
import com.itheima.advantagesup.entity.SysMenu;
import com.itheima.advantagesup.service.SysMenuService;
import com.itheima.advantagesup.vo.MenuVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/sys_menu")
@RequiredArgsConstructor
@Slf4j
public class SysMenuController {

    private final SysMenuService sysMenuService;


    /**
     * 菜单管理新增
     *
     * @param sysMenuDTO
     * @return {@link R}
     */
    @PostMapping("/add")
    public R<?> add(@RequestBody SysMenuDTO sysMenuDTO){
        SysMenu sysMenu = new SysMenu();
        // ===========================
        String component = sysMenuDTO.getComponent();
        String icon = sysMenuDTO.getIcon();
        Integer is_cache = sysMenuDTO.getIs_cache();
        Integer is_link = sysMenuDTO.getIs_link();
        Integer is_show = sysMenuDTO.getIs_show();
        String mark = sysMenuDTO.getMark();
        String path = sysMenuDTO.getPath();
        Integer pid = sysMenuDTO.getPid();
        String redirect = sysMenuDTO.getRedirect();
        BigDecimal sort = sysMenuDTO.getSort();
        String title = sysMenuDTO.getTitle();
        Integer type = sysMenuDTO.getType();

        // ===========================
        sysMenu.setPid(pid);
        sysMenu.setPath(path);
        sysMenu.setComponent(component);
        sysMenu.setTitle(title);
        sysMenu.setIcon(icon);
        sysMenu.setIsShow(is_show);
        sysMenu.setIsCache(is_cache);
        sysMenu.setIsLink(is_link);
        sysMenu.setRedirect(redirect);
        sysMenu.setCreateDate(LocalDateTime.now());
        sysMenu.setUpdateDate(LocalDateTime.now());
        sysMenu.setDelFlag(0);
        sysMenu.setType(type);
        sysMenu.setSort(sort);
        sysMenu.setMark(mark);



        sysMenuService.add(sysMenu);
        return R.success();
    }


    /**
     * 根据id删除数据
     *
     * @param id
     * @return {@link R}<{@link ?}>
     */
    @GetMapping("/del")
    public R<?> delete(Integer id){
        sysMenuService.delete(id);
        return R.success();
    }


    @GetMapping("/rowInfo")
    public R<SysMenu> selectById(Integer id){

        SysMenu sysMenu = sysMenuService.selectById(id);

        return R.success(sysMenu);

    }
    /**
     * 修改菜单信息
     * @param
     * @return {@link R}<{@link ?}>
     */
    @PostMapping("/update")
    public R<?> update(@RequestBody SysMenu sysMenu){
      /*  SysMenu sysMenu = new SysMenu();
        // ===========================
        String component = sysMenuDTO.getComponent();
        String icon = sysMenuDTO.getIcon();
        Integer is_cache = sysMenuDTO.getIs_cache();
        Integer is_link = sysMenuDTO.getIs_link();
        Integer is_show = sysMenuDTO.getIs_show();
        String mark = sysMenuDTO.getMark();
        String path = sysMenuDTO.getPath();
        Integer pid = sysMenuDTO.getPid();
        String redirect = sysMenuDTO.getRedirect();
        BigDecimal sort = sysMenuDTO.getSort();
        String title = sysMenuDTO.getTitle();
        Integer type = sysMenuDTO.getType();
        Integer id = sysMenuDTO.getId();

        // ===========================
        sysMenu.setId(id);
        sysMenu.setPid(pid);
        sysMenu.setPath(path);
        sysMenu.setComponent(component);
        sysMenu.setTitle(title);
        sysMenu.setIcon(icon);
        sysMenu.setIsShow(is_show);
        sysMenu.setIsCache(is_cache);
        sysMenu.setIsLink(is_link);
        sysMenu.setRedirect(redirect);
        sysMenu.setCreateDate(LocalDateTime.now());
        sysMenu.setUpdateDate(LocalDateTime.now());
        sysMenu.setDelFlag(0);
        sysMenu.setType(type);
        sysMenu.setSort(sort);
        sysMenu.setMark(mark);
*/
        sysMenuService.update(sysMenu);
        return R.success();
    }

    @GetMapping("allData")
    public R<List<MenuVO>> allData(){
        List<MenuVO> menuVOList =  sysMenuService.allData();
        return R.success(menuVOList);


    }






}
