package com.itheima.advantagesup.controller;


import com.itheima.advantagesup.common.PageBean;
import com.itheima.advantagesup.common.R;
import com.itheima.advantagesup.dto.SysRoleDTO;
import com.itheima.advantagesup.entity.SysRole;
import com.itheima.advantagesup.service.SysRoleService;
import com.itheima.advantagesup.vo.SysRoleVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.bind.annotation.*;

import java.net.PortUnreachableException;
import java.util.List;

@RestController
@RequestMapping("/sys_role")
@RequiredArgsConstructor
@Slf4j
public class SysRoleController {
    private  final SysRoleService sysRoleService;

    /**
     * 角色列表分页查询
     *
     * @param page
     * @param limit
     * @param name
     * @return {@link R}<{@link PageBean}<{@link SysRoleVO}>>
     */
    @GetMapping("/list")
    public R<PageBean<SysRoleVO>> list(@RequestParam(defaultValue = "1") Integer page,
                     @RequestParam(defaultValue = "10") Integer limit,
                     String name){
        PageBean<SysRoleVO> pageBean = sysRoleService.list(page,limit,name);
        return R.success(pageBean);

    }

    /**
     * 查询所有数据
     *
     * @return {@link R}<{@link List}<{@link SysRoleVO}>>
     */
    @GetMapping("/allData")
    public R<List<SysRoleVO>> allData(){
        List<SysRoleVO> sysRoleVOList = sysRoleService.allData();
        return R.success(sysRoleVOList);

    }

    /**
     * 角色添加
     *
     * @param sysRoleDTO
     * @return {@link R}<{@link ?}>
     */
    @PostMapping("/add")
    public R<?> add(@RequestBody SysRoleDTO sysRoleDTO){
        sysRoleService.add(sysRoleDTO);
        return R.success();

    }

    //查询回显
    @GetMapping("/rowInfo")
    public R<SysRoleVO> rowInfo(String id){
        SysRoleVO sysRoleVO = sysRoleService.rowInfo(id);
        return R.success(sysRoleVO);
    }

    /**
     *
     * 数据更新
     *
     *
     * @param sysRoleDTO
     * @return {@link R}<{@link ?}>
     */
    @PostMapping("/update")
    public R<?> update(@RequestBody SysRoleDTO sysRoleDTO){
        sysRoleService.update(sysRoleDTO);
        return R.success();

    }

    @GetMapping("/del")
    public R<?> del(String id){
        sysRoleService.del(id);
        return R.success();
    }










}
