package com.itheima.advantagesup.mapper;

import com.itheima.advantagesup.entity.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * SysRoleMenuMapper
 *
 * @author liudo
 * @version 1.0
 * @project advantages-up
 * @date 2023/9/11 21:46:47
 */
@Mapper
public interface SysRoleMenuMapper {
    /**
     * 插入批次
     *
     * @param sysRoleMenuList sys角色菜单列表
     */
    void insertBatch(List<SysRoleMenu> sysRoleMenuList);

    /**
     * 按角色id删除
     *
     * @param roleId roleId
     */
    void deleteByRoleId(String roleId);
}
