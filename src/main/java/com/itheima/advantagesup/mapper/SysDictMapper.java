package com.itheima.advantagesup.mapper;

import com.itheima.advantagesup.entity.SysDict;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * SysDictMapper
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 18:39
 */
@Mapper
public interface SysDictMapper {
    /**
     * 选择列表
     *
     * @return {@link List}<{@link SysDict}>
     */
    List<SysDict> selectList();
}
