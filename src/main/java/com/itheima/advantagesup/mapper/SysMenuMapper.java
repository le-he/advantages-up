package com.itheima.advantagesup.mapper;

import com.itheima.advantagesup.entity.SysMenu;
import com.itheima.advantagesup.vo.MenuVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * SysMenuMapper
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 17:57
 */
@Mapper
public interface SysMenuMapper {




    /**
     * 选择列表按角色id
     *
     * @param roleIds 角色ids
     * @return {@link List}<{@link SysMenu}>
     */
    List<SysMenu> selectListByRoleIds(List<String> roleIds);


    /**
     * 增加数据
     *
     * @param sysMenu
     */
    void insert(SysMenu sysMenu);

    /**
     * 根据删除数据
     *
     * @param id
     */
    @Delete("delete from sys_menu where id = #{id}")
    void delete(Integer id);

    void update(SysMenu sysMenu);

    @Select("select * from sys_menu where pid = #{pid}")
    List<SysMenu> selectListByPid(int pid);

    @Select("select * from sys_menu where id = #{id}")
    SysMenu selectById(Integer id);
}
