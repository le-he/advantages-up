package com.itheima.advantagesup.mapper;


import com.itheima.advantagesup.entity.SysRole;
import com.itheima.advantagesup.vo.SysRoleVO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * SysRoleMapper
 *
 * @author liliudong
 * @version 1.0
 * @description
 * @date 2023/7/26 17:51
 */
@Mapper
public interface SysRoleMapper {
    /**
     * 选择名单由ids
     *
     * @param roleIds 角色id
     * @return {@link List}<{@link String}>
     */
    List<String> selectNameListByIds(List<String> roleIds);


    List<SysRole> selectList(SysRole sysRole);

    @Select("select * from sys_role")
    List<SysRoleVO> selectAllData();

    void insert(SysRole sysRole);

    @Select("select * from sys_role where id = #{id}")
    SysRole selectById(String id);

    void update(SysRole sysRole);

    @Delete("delete from sys_role where id = #{id}")
    void delete(String id);
}
